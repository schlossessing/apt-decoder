class FIRFilter {
  
  // declare a variable 'coefficients' of datatype 'double'
  // a coefficient is a numerical or constant quantity placed before and multiplying the variable... 
  //in an algebraic expression (e.g. 4 in 4x y)
  double[] coefficients;
  
  // declare a variable 'offset' of datatype 'int'
  int offset;
  
  // declare a variable 'center' of datatype 'double'
  double center;
  
  // declare a variable 'curSamples' of datatype 'double'
  double[] curSamples;
  
  FIRFilter(double[] coefficients) {
    
    // 'this' refers to the current object
    this.coefficients = coefficients;
    offset = coefficients.length - 1;
    center = Math.floor(coefficients.length / 2);
    curSamples = new double[offset];
  }
  
  void loadSamples(double[] samples) {
    double[] newSamples = new double[samples.length + offset];

    double[] subCurSamples = (double[]) subset(curSamples, curSamples.length - offset);
    arrayCopy(subCurSamples, newSamples);
    arrayCopy(samples, 0, newSamples, offset, samples.length);
    
    curSamples = newSamples;
  }
  
  double get(int index) {
    double value = 0;
    
    for (int i = 0 ; i < coefficients.length ; i++) {
      value += coefficients[i] * curSamples[index + i];
    }
    return value;
  }
}
