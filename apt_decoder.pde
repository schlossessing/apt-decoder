//hi Flo :) looking forward to working on this project with you!

import processing.sound.*;

//A String is a sequence of characters
//A String is an Object 
// Strings are always defined inside double quotes 
// this String pulls a named WAV file that the user has created from a satellite pass
String fileName = "137.100_2021-06-04_18-38-24-3_resampled.wav";

// create an Array called 'frames' of datatype 'float'
float[] frames;

//Declare variable 'numTaps' of datatype 'int' (whole numbers)
// what is numTaps?
// numTaps has to do with filtering. 
// A FIR filter or 'Finite Impulse Response  Filter' takes n samples from the data stream and n coefficients
// and multiplies them together and then sums up all of those products
// The number of taps is just what n is.
// The more taps, the better the filter, but also the slower it runs.
int numTaps = 50;

// declare variable 'SignalMean' of type 'double' 
// Double is a Datatype for floating-point numbers larger than those that can be stored in a float
double signalMean = 0;

// create an Array called 'coeffs' of datatype 'double'; 
// the x value is the sample rate of the WAV file (11025 Hz); 
//the y value of 1200 is 'half the amplitude of the frequency' as seen below; 
// this makes sense because the visual info is encoded in a 2400 Hz signal
// the z value is numTaps, which was declared above as being = 50 (50 samples of the data stream)
double[] coeffs = getLowPassFirCoeffs(11025, 1200, numTaps);

//Declare new class 'FIRFilter' and construct object 'filter'
// So the above array called 'coeffs' is part of the 'constructor' of the new class 'FRIFilter'?
FIRFilter filter = new FIRFilter(coeffs);

// create an Array called 'filteredData' of datatype 'double';
// this array is what the computer gives us after it runs the FIRFilter?
double[] filteredData;

// create an Array called 'getLowPassFirCoeffs' of datatype 'double';
// as declared above, 'getLowPassFirCoeffs' has three parts; x = samplerate, 
// y = half ampplitude frequency which is 1200
// the z value is numTaps which was declared above as 50 
// inside the {} is the 'array value'
// this whole next section is calculating the FIR coefficients that we need to compute the Hilbert Transform
double[] getLowPassFirCoeffs(float sampleRate, float halfAmplFreq, int numTaps) {
  
  // numTaps = numTaps + whatever is left as a 'remainder' after the division of (numTaps + 1) / 2
  // += is 'add assign' or add numTaps + (numTaps + 1) % 2
  numTaps += (numTaps + 1) % 2;
  // declares a variable 'freq' of type 'double' that is equal to halAmplFreq / sampleRate
  // 1200 cycles / second * 1 second / 11025 samples 
  double freq = halfAmplFreq / sampleRate;
  
  double[] coeffs = new double[numTaps];
  // 'floor' Calculates the closest int value that is less than or equal to the value of the parameter
  // declares a varable 'center' of type 'double' that is the math.floor of numTaps/2
  double center = Math.floor(numTaps / 2);
  float sum = 0;
 
 // a for loop that iterates until 'numTaps' has been reached 
  for (int i = 0 ; i < numTaps ; i++) {
    double val;
    // if i is equal to center
    if (i == center) {
      // declare a variable 'val' that has the below equivalence
      // val is 2 * pi (3.141592) * frequency 
      // val is the angular frequency of the signal measured in radians/sec, which is described in the equation 
      // Wc = 2 * pi * Fc where Wc is the angular frequency of the signal measured in radians/sec
      val = 2 * Math.PI * freq;
      // otherwise
    } else {
      // the variable 'angle' is equal to 2 times pi times (the numTap int + 1) / (numTaps + 1)
      double angle = 2 * Math.PI * (i + 1) / (numTaps + 1);
      // print what the angle is 
      println("ANGLE -> "+ angle);
      // val is equal to an angle (in radians)
      val = Math.sin(2 * Math.PI * freq * (i - center)) / (i - center);
      // val is equal to val times 0.42 - 0.5 * Math.cos(angle) + 0.08 * Math.cos(2 * angle)
      val *= 0.42 - 0.5 * Math.cos(angle) + 0.08 * Math.cos(2 * angle);
      println("VAL -> "+ val);
    }
    // sum is equal to sum + val
    sum += val;
    coeffs[i] = val;
  }

// question: what does it mean if the '++' comes before the 'i'?
  for (int i = 0; i < numTaps; ++i) {
    // /= is 'divide assign'. The expression a /= b is equivalent to a = a / b
    // coeffs[i] = coeffs[i] / sum 
    coeffs[i] /= sum;
  }
  
  return coeffs;  
}  

void setup() {
  size(1920, 1000);
  background(255, 180, 220);
 
// Declare new class 'SoundFile' and construct object 'file'
// "this" references the Processing sketch
  SoundFile file = new SoundFile(this, fileName);
  println(file.sampleRate());

// frames is equal to number of frames in the file 
  frames = new float[file.frames()];
  file.read(frames);

// a for loop that iterates until the limit of the max frames in the file 
  for (int i=0 ; i < frames.length ; i++) {
    // 'abs' Calculates the absolute value (magnitude) of a number. The absolute value of a number is always positive. 
    frames[i] = Math.abs(frames[i]);
  }
  
  filterSamples();
}

// a function 'normalize' that returns no value (hence use of 'void')
void normalize() {
  // why is the maxVal 0 and the minVal 1?
  double maxVal = 0;
  double minVal = 1;
  
  // a for loop that iterates until the length of the filteredData has been reached
  for(int i = 0; i < filteredData.length; i++) {
    // if filteredData is greater than maxVal (0)
    if(filteredData[i] > maxVal){
      // set filteredData to 0
      maxVal = filteredData[i];
    }
    // if filtered data is less than 1
    if(filteredData[i] < minVal){
      // set filteredData to 1 
      minVal = filteredData[i];
    }
  }
  
  for(int i = 0; i < filteredData.length; i++) {
    filteredData[i] = (filteredData[i] - minVal) / (maxVal - minVal);
    signalMean += filteredData[i];
  }
  
  signalMean = signalMean / filteredData.length;
}

void filterSamples() {
  double[] fs = new double[frames.length];
  for (int i=0 ; i < frames.length ; i++) {
    // what is 32768 doing here?
    // 32,768 = 2^15 = pow(2, 15)
    // The wav file is integers raining from +32,767 to -32,768 so to convert to floating point 
    // between plus and minus 1 need to divide by 32,768.
    fs[i] = frames[i] / 32768;
  }
  
  filter.loadSamples(fs);
  
  filteredData = new double[fs.length];
  for (int i = 0 ; i < fs.length ; i++) {
    filteredData[i] = filter.get(i);
  }
  
  normalize();
  // create variable 'map' of type FloatDict 
  // a 'FloatDict' is a simple class to use a String as a lookup for a float value
  // 22050 = 11025 * 2 = samplerate * 2
  FloatDict map = convolveWithSync(0, 22050);
  
  // Creates a new PImage (the datatype for storing images)
  createImage((int)map.get("index"));
}

// what is this??
FloatDict convolveWithSync(int start, int range) {
  int[] sync = { 
    -1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, -1, -1, -1, 
    -1, -1, 1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, -1, 
    -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, 1, 1, 1, 
    1, 1, 1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, 
    1, 1, 1, 1, 1
  };
  
  float maxVal = 0;
  float maxIndex = 0;
  
  for (int i = start; i < start + range; i++) {
    float sum = 0;
    
    for (int c = 0; c < sync.length; c++) {
      int index = i + c;
      // clamping the index
      if (index > filteredData.length) {
        index = filteredData.length - 1;
      }
      sum += (filteredData[index] - signalMean) * sync[c];
    }
    
    if (sum > maxVal) {
      maxVal = sum;
      maxIndex = i;
    }
  }
  
  FloatDict res = new FloatDict();
  res.set("index", maxIndex);
  res.set("score", maxVal);
  return res;
}

void createImage(int startingIndex) {
  int pixelScale = 3;
  
  // the sample rate is 11025 samples / second and according to the APT specification, each line
  // is 0.5 seconds long. That means 11025 / 2 = 5512.5 samples / line
  // 5512.5 samples / line rounds up to 5513
  int lineCount = (int) Math.floor(filteredData.length/5513) / pixelScale;
  PImage image = createImage(1040, lineCount, ARGB);
  image.loadPixels();
  
  int lineStartIndex = startingIndex;
  println(lineCount, " possible lines");
  
  // Declare new class 'DownSampler' and construct object 'downsampler' with three elements in its construction
  // earlier we saw: double[] coeffs = getLowPassFirCoeffs(11025, 1200, numTaps); 
  // so 'coeffs' is of datatype 'double' and is an array
  // this new class Downsampler is defined by the WAV file's samplerate (11025), a number 4160 and the array 'coeffs'
  // 1040 * 4 = 4160
  DownSampler downSampler = new DownSampler(11025, 4160, coeffs);
  double[] lineData;
  
  for(int line = 0; line < lineCount; line++) {
    println("line -> " + line);
    println("lineStartIndex -> ", lineStartIndex);
    
    // 5533 - 20 is 5513 (the rounded up number of samples per line of APT data)
    int stopIndex = lineStartIndex + 5533;
    
    // need to clamp to a valid index range...
    // does clamp mean 'sync'?
    if (stopIndex >= filteredData.length) {
      stopIndex = filteredData.length - 1;
    }
    int startIndex = lineStartIndex + 20;
    int count = (stopIndex - startIndex) + 1;
    
    // need to clamp to a valid index range...
    if (count < 0) {
      startIndex = 0;
      count = 0;
    }
    
    println("count: ", count);
    double[] bla = (double[]) subset(filteredData, startIndex, count);
    lineData = downSampler.downSample(bla);
    
    // a for loop that builds the pixels in the image until 1040 has been reached
    for(int column = 0; column < 1040; column++) {
      int index = 0 + column * pixelScale;
      
      if (lineData.length > 0) { 
        if (index >= lineData.length) {
          index = lineData.length - 1;
        }
        // 256 refers to the number of 8 bit colour values 0 - 255 (?)
        double value = lineData[index] * 256;
        //println(value);
        
        // 'color' has three values plus a fourth = alphatransparency
        image.pixels[line * 1040 + column] = color((int)value, (int)value, (int)value, 255);
      } else {
        image.pixels[line * 1040 + column] = color(0, 0, 0, 255);
      }
    }
    
    // 5512 is the number of samples per line of APT data rounded down from 5512.5
    FloatDict conv = convolveWithSync(lineStartIndex + (5512 * pixelScale) - 20, 40);
    if(conv.get("score") > 6) {
      lineStartIndex = (int) conv.get("index");
    } else {
      lineStartIndex += 5512 * pixelScale;
    }
  }
  
  image.updatePixels();
  image(image, 0, 0);
  
  textSize(40);
  text("where am I?", 1100, 100);
}
