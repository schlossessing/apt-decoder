// Is this Downsampler reducing the samplerate further? 
// this is something that Dmitrii Eliuseev does in his Python version of a satellite decoder   

class DownSampler {
  
  // declare an object 'filter' of class FIRFilter
  FIRFilter filter;
  // declare a variable 'rateMul' of datatype float 
  float rateMul;
 
  
  DownSampler(int inRate, int outRate, double[] coefficients) {
    filter = new FIRFilter(coefficients);
    rateMul = inRate / outRate;
  }
  
  double[] downSample(double[] samples) {
    filter.loadSamples(samples);
    double[] out = new double[(int) Math.floor(samples.length / rateMul)];
    
    for (int i = 0, readFrom = 0; i < out.length; ++i, readFrom += rateMul) {
      out[i] = filter.get((int) Math.floor(readFrom));
    }
    
    return out;
  }
}
